<!-- MUHAMMAD DIMAS ANDHIKA -->
<!-- TUGAS 9 -->
<!-- 12/11/2020 -->

<?php

    require_once 'animal.php';

    class Ape extends Animal
    {
        function __construct($name)
        {
            parent::__construct($name);
        }

        function yell()
        {
            echo 'Auooo';
        }
    }

?>

<!-- MUHAMMAD DIMAS ANDHIKA -->
<!-- TUGAS 9 -->
<!-- 12/11/2020 -->