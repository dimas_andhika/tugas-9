<!-- MUHAMMAD DIMAS ANDHIKA -->
<!-- TUGAS 9 -->
<!-- 12/11/2020 -->

<?php

	require_once 'animal.php';
	require_once 'ape.php';
	require_once 'frog.php';

	$sheep = new Animal("shaun");
	echo 'Animal : ' . $sheep->get_name() . '<br>'; // "shaun"
	echo 'Legs : ' . $sheep->get_legs() . '<br>'; // 2
	echo 'Cold Blooded : ' . $sheep->get_cold_blooded() . '<br><br>'; // false
	// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

	$sungokong = new Ape("kera sakti");
	echo 'Nama Ape : ' . $sungokong->get_name() . '<br>'; // "kera sakti"
	echo 'Legs : ' . $sungokong->get_legs() . '<br>'; // 2
	echo 'Cold Blooded : ' . $sungokong->get_cold_blooded() . '<br>'; // false
	echo 'Yell : ' ;
	$sungokong->yell(); // "Auooo"
	echo '<br><br>';

	$kodok = new Frog("buduk");
	echo 'Animal : '.$kodok->get_name() . '<br>'; // "buduk"
	echo 'Legs : '.$kodok->get_legs() . '<br>'; // 4
	echo 'Cold Blooded : '.$kodok->get_cold_blooded() . '<br>'; // false
	echo 'Jump : ' ;
	$kodok->jump(); // "hop hop"
	echo '<br><br>';

?>

<!-- MUHAMMAD DIMAS ANDHIKA -->
<!-- TUGAS 9 -->
<!-- 12/11/2020 -->
