<!-- MUHAMMAD DIMAS ANDHIKA -->
<!-- TUGAS 9 -->
<!-- 12/11/2020 -->

<?php

    require_once 'animal.php';

    class Frog extends Animal
    {
        function __construct($name)
        {
            parent::__construct($name);
            $this->legs = 4;
        }

        function jump()
        {
            echo 'hop hop';
        }
    }
    
?>

<!-- MUHAMMAD DIMAS ANDHIKA -->
<!-- TUGAS 9 -->
<!-- 12/11/2020 -->