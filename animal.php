<!-- MUHAMMAD DIMAS ANDHIKA -->
<!-- TUGAS 9 -->
<!-- 12/11/2020 -->

<?php

	class Animal
	{
		public $name;
		public $legs;
		public $cold_blooded;
				
		function __construct($name)
		{
			$this->name = $name;
			$this->legs = 2;
			$this->cold_blooded = "false";
		}
		
		function get_name()
		{
			return $this->name;
		}

		function get_legs()
		{
			return $this->legs;
		}

		function get_cold_blooded()
		{
			return $this->cold_blooded;
		}
	}
	
?>

<!-- MUHAMMAD DIMAS ANDHIKA -->
<!-- TUGAS 9 -->
<!-- 12/11/2020 -->